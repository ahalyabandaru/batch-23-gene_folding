#implementation of GOOF(Genetically Optimized Organic Folding) algorithm 

def apply_goof(sequence):
    while True:
        original_sequence = sequence
        length = len(sequence)//2
        while length!=-1:
            if sequence[:length][::-1]==sequence[length:length+length]:
                sequence = sequence[length:]
                length=-1
            else:
                length-=1
        sequence = sequence[::-1]

        length=len(sequence)//2
        while length!=-1:
            if sequence[:length][::-1]==sequence[length:length+length]:
                sequence = sequence[length:]
                length=-1
            else:
                length-=1
        sequence = sequence[::-1]

        if sequence==original_sequence:
            break
    return len(sequence)

#testing with list of inputs

sequences = ["AATTACC", "AAAAGAATTAA"]
for sequence in sequences :
    print(apply_goof(sequence))
        